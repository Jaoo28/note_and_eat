// ignore: file_names
import 'package:flutter/material.dart';

class ColorsPallete {
  static const darkGreen = Color.fromRGBO(0, 60, 45, 1);
  static const orange = Color.fromRGBO(254, 130, 8, 1);
  static const black = Color.fromRGBO(57, 57, 57, 1);
  static const black50Opacity = Color.fromRGBO(57, 57, 57, 0.5);
  static const black75Opacity = Color.fromRGBO(57, 57, 57, 0.75);
  static const grey = Color.fromRGBO(133, 133, 133, 1);
  static const lightGrey = Color.fromRGBO(230, 230, 230, 1);
  static const whitePastel = Color.fromRGBO(229, 229, 229, 1);
  static const white = Colors.white;

  static const primaryColor = Color(0xFF685BFF);
  static const canvasColor = Color(0xFF2E2E48);
  static const scaffoldBackgroundColor = Color(0xFF464667);
  static const accentCanvasColor = Color(0xFF3E3E61);
}

Color actionColor = const Color(0xFF5F5FA7).withOpacity(0.6);
Divider divider =
    Divider(color: ColorsPallete.white.withOpacity(0.3), height: 1);
