import 'dart:developer';

import 'package:mobx/mobx.dart';

part 'select_product_type_store.g.dart';

class SelectProductTypeStore = _SelectProductTypeStore with _$SelectProductTypeStore;

abstract class _SelectProductTypeStore with Store {
  @observable
  String selectedTypeProduct = ''; 

  @action
  void change(String type) {
     log('Filtro antes de alterar: Texto: ${selectedTypeProduct}');
    selectedTypeProduct = type; 
     log('Filtro alterado para: Texto: ${selectedTypeProduct}');
  }

  @computed
  String get productTypeGet => selectedTypeProduct;

}

