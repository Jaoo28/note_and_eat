// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'select_product_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$SelectProductStore on _SelectProductStore, Store {
  Computed<bool>? _$isHoverGetComputed;

  @override
  bool get isHoverGet =>
      (_$isHoverGetComputed ??= Computed<bool>(() => super.isHoverGet,
              name: '_SelectProductStore.isHoverGet'))
          .value;
  Computed<int>? _$indexCardGetComputed;

  @override
  int get indexCardGet =>
      (_$indexCardGetComputed ??= Computed<int>(() => super.indexCardGet,
              name: '_SelectProductStore.indexCardGet'))
          .value;
  Computed<String>? _$getProductSizeComputed;

  @override
  String get getProductSize =>
      (_$getProductSizeComputed ??= Computed<String>(() => super.getProductSize,
              name: '_SelectProductStore.getProductSize'))
          .value;
  Computed<int>? _$getProductCounterComputed;

  @override
  int get getProductCounter => (_$getProductCounterComputed ??= Computed<int>(
          () => super.getProductCounter,
          name: '_SelectProductStore.getProductCounter'))
      .value;
  Computed<String>? _$getTotalAmountComputed;

  @override
  String get getTotalAmount =>
      (_$getTotalAmountComputed ??= Computed<String>(() => super.getTotalAmount,
              name: '_SelectProductStore.getTotalAmount'))
          .value;
  Computed<List<String>>? _$getOptionalsCheckedComputed;

  @override
  List<String> get getOptionalsChecked => (_$getOptionalsCheckedComputed ??=
          Computed<List<String>>(() => super.getOptionalsChecked,
              name: '_SelectProductStore.getOptionalsChecked'))
      .value;

  late final _$ishoverAtom =
      Atom(name: '_SelectProductStore.ishover', context: context);

  @override
  bool get ishover {
    _$ishoverAtom.reportRead();
    return super.ishover;
  }

  @override
  set ishover(bool value) {
    _$ishoverAtom.reportWrite(value, super.ishover, () {
      super.ishover = value;
    });
  }

  late final _$indexCardAtom =
      Atom(name: '_SelectProductStore.indexCard', context: context);

  @override
  int get indexCard {
    _$indexCardAtom.reportRead();
    return super.indexCard;
  }

  @override
  set indexCard(int value) {
    _$indexCardAtom.reportWrite(value, super.indexCard, () {
      super.indexCard = value;
    });
  }

  late final _$productSizeAtom =
      Atom(name: '_SelectProductStore.productSize', context: context);

  @override
  String get productSize {
    _$productSizeAtom.reportRead();
    return super.productSize;
  }

  @override
  set productSize(String value) {
    _$productSizeAtom.reportWrite(value, super.productSize, () {
      super.productSize = value;
    });
  }

  late final _$productCounterAtom =
      Atom(name: '_SelectProductStore.productCounter', context: context);

  @override
  int get productCounter {
    _$productCounterAtom.reportRead();
    return super.productCounter;
  }

  @override
  set productCounter(int value) {
    _$productCounterAtom.reportWrite(value, super.productCounter, () {
      super.productCounter = value;
    });
  }

  late final _$totalAmountAtom =
      Atom(name: '_SelectProductStore.totalAmount', context: context);

  @override
  String get totalAmount {
    _$totalAmountAtom.reportRead();
    return super.totalAmount;
  }

  @override
  set totalAmount(String value) {
    _$totalAmountAtom.reportWrite(value, super.totalAmount, () {
      super.totalAmount = value;
    });
  }

  late final _$optionalsCheckedAtom =
      Atom(name: '_SelectProductStore.optionalsChecked', context: context);

  @override
  List<String> get optionalsChecked {
    _$optionalsCheckedAtom.reportRead();
    return super.optionalsChecked;
  }

  @override
  set optionalsChecked(List<String> value) {
    _$optionalsCheckedAtom.reportWrite(value, super.optionalsChecked, () {
      super.optionalsChecked = value;
    });
  }

  late final _$_SelectProductStoreActionController =
      ActionController(name: '_SelectProductStore', context: context);

  @override
  void change(bool response, int hoverIndex) {
    final _$actionInfo = _$_SelectProductStoreActionController.startAction(
        name: '_SelectProductStore.change');
    try {
      return super.change(response, hoverIndex);
    } finally {
      _$_SelectProductStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void changeProductSize(String size) {
    final _$actionInfo = _$_SelectProductStoreActionController.startAction(
        name: '_SelectProductStore.changeProductSize');
    try {
      return super.changeProductSize(size);
    } finally {
      _$_SelectProductStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addProductQuantity(String price) {
    final _$actionInfo = _$_SelectProductStoreActionController.startAction(
        name: '_SelectProductStore.addProductQuantity');
    try {
      return super.addProductQuantity(price);
    } finally {
      _$_SelectProductStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void removeProductQuantity(String price) {
    final _$actionInfo = _$_SelectProductStoreActionController.startAction(
        name: '_SelectProductStore.removeProductQuantity');
    try {
      return super.removeProductQuantity(price);
    } finally {
      _$_SelectProductStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addOptional(String name) {
    final _$actionInfo = _$_SelectProductStoreActionController.startAction(
        name: '_SelectProductStore.addOptional');
    try {
      return super.addOptional(name);
    } finally {
      _$_SelectProductStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void removeOptional(String name) {
    final _$actionInfo = _$_SelectProductStoreActionController.startAction(
        name: '_SelectProductStore.removeOptional');
    try {
      return super.removeOptional(name);
    } finally {
      _$_SelectProductStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void resetValues() {
    final _$actionInfo = _$_SelectProductStoreActionController.startAction(
        name: '_SelectProductStore.resetValues');
    try {
      return super.resetValues();
    } finally {
      _$_SelectProductStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
ishover: ${ishover},
indexCard: ${indexCard},
productSize: ${productSize},
productCounter: ${productCounter},
totalAmount: ${totalAmount},
optionalsChecked: ${optionalsChecked},
isHoverGet: ${isHoverGet},
indexCardGet: ${indexCardGet},
getProductSize: ${getProductSize},
getProductCounter: ${getProductCounter},
getTotalAmount: ${getTotalAmount},
getOptionalsChecked: ${getOptionalsChecked}
    ''';
  }
}
