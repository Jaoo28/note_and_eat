import 'dart:developer';

import 'package:mobx/mobx.dart';

part 'select_product_store.g.dart';

class SelectProductStore = _SelectProductStore with _$SelectProductStore;

abstract class _SelectProductStore with Store {
  @observable
  var ishover = false;

  @observable
  var indexCard = -1;

  @observable
  var productSize = "M";

  @observable
  var productCounter = 1;

  @observable
  var totalAmount = "";

  @observable
  List<String> optionalsChecked = [];

  @action
  void change(bool response, int hoverIndex) {
    indexCard = hoverIndex;
    ishover = response;
  }

  @action
  void changeProductSize(String size) {
    productSize = size;
    log('Size: $productSize');
  }

  @action
  void addProductQuantity(String price) {
    double priceItem = double.parse(price);
    double priceAmount;

    productCounter++;
    priceAmount = (priceItem * getProductCounter);
    totalAmount = priceAmount.toStringAsFixed(2);

    log('Quantity: $getProductCounter');
    log('Total: $getTotalAmount');
  }

  @action
  void removeProductQuantity(String price) {
    double priceItem = double.parse(price);
    double priceAmount;
    if (productCounter != 1) {
      productCounter--;
      priceAmount = (priceItem * getProductCounter);
      totalAmount = priceAmount.toStringAsFixed(2);
    }
    log('Quantity: $getProductCounter');
    log('Total: $getTotalAmount');
  }

  @action
  void addOptional(String name) {
    optionalsChecked.add(name);
    log("Add: $name, List: $getOptionalsChecked");
  }

  @action
  void removeOptional(String name) {
    optionalsChecked.remove(name);
    log("Remove: $name, List: $getOptionalsChecked");
  }

  @action
  void resetValues() {
    productCounter = 1;
    totalAmount = "";
    productSize = "M";
  }

  @computed
  bool get isHoverGet => ishover;

  @computed
  int get indexCardGet => indexCard;

  @computed
  String get getProductSize => productSize;

  @computed
  int get getProductCounter => productCounter;

  @computed
  String get getTotalAmount => totalAmount;

  @computed
  List<String> get getOptionalsChecked => optionalsChecked;
}
