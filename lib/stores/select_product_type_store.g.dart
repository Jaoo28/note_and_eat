// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'select_product_type_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$SelectProductTypeStore on _SelectProductTypeStore, Store {
  Computed<String>? _$productTypeGetComputed;

  @override
  String get productTypeGet =>
      (_$productTypeGetComputed ??= Computed<String>(() => super.productTypeGet,
              name: '_SelectProductTypeStore.productTypeGet'))
          .value;

  late final _$selectedTypeProductAtom = Atom(
      name: '_SelectProductTypeStore.selectedTypeProduct', context: context);

  @override
  String get selectedTypeProduct {
    _$selectedTypeProductAtom.reportRead();
    return super.selectedTypeProduct;
  }

  @override
  set selectedTypeProduct(String value) {
    _$selectedTypeProductAtom.reportWrite(value, super.selectedTypeProduct, () {
      super.selectedTypeProduct = value;
    });
  }

  late final _$_SelectProductTypeStoreActionController =
      ActionController(name: '_SelectProductTypeStore', context: context);

  @override
  void change(String type) {
    final _$actionInfo = _$_SelectProductTypeStoreActionController.startAction(
        name: '_SelectProductTypeStore.change');
    try {
      return super.change(type);
    } finally {
      _$_SelectProductTypeStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
selectedTypeProduct: ${selectedTypeProduct},
productTypeGet: ${productTypeGet}
    ''';
  }
}
