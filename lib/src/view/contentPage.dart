import 'package:anota_ai/src/view/homePage.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sidebarx/sidebarx.dart';

import '../../shared/constanst.dart';
import 'cardapio.dart';

class contentPage extends StatelessWidget {
  const contentPage({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final SidebarXController controller;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final pageTitle = _getTitleByIndex(controller.selectedIndex);
    final isBigScreen = MediaQuery.of(context).size.width >= 600;
    return AnimatedBuilder(
      animation: controller,
      builder: (context, child) {
        final pageTitle = _getTitleByIndex(controller.selectedIndex);
        switch (controller.selectedIndex) {
          case 0:
            return CardapioPage();
          // return Text(
          //   pageTitle,
          //   style: theme.textTheme.headline5,
          // );
          case 1:
            return CardapioPage();
          case 2:
            return Text(
              pageTitle,
              style: theme.textTheme.headline5,
            );
          case 3:
            return Text(
              pageTitle,
              style: theme.textTheme.headline5,
            );
          case 4:
            return Text(
              pageTitle,
              style: theme.textTheme.headline5,
            );

          default:
            return Text(
              pageTitle,
              style: theme.textTheme.headline5,
            );
        }
      },
    );
  }
}

String _getTitleByIndex(int index) {
  switch (index) {
    case 0:
      return 'Home';
    case 1:
      return 'Cardápio';
    case 2:
      return 'Meus Pedidos';
    case 3:
      return 'Carrinho';
    default:
      return 'Not found page';
  }
}
