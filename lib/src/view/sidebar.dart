import 'package:flutter/material.dart';
import 'package:anota_ai/shared/constanst.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sidebarx/sidebarx.dart';
import 'package:google_fonts/google_fonts.dart';

class SideBar extends StatelessWidget {
  const SideBar({
    Key? key,
    required SidebarXController controller,
  })  : _controller = controller,
        super(key: key);

  final SidebarXController _controller;

  @override
  Widget build(BuildContext context) {
    return SidebarX(  
      controller: _controller,
      theme: SidebarXTheme( 
        margin: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: ColorsPallete.white,
          borderRadius: BorderRadius.circular(20),
        ),
        textStyle: GoogleFonts.ubuntu(
          textStyle: const TextStyle(
            color: ColorsPallete.grey,
            fontWeight: FontWeight.normal,
            fontSize: 16,
          ),
        ),
        selectedTextStyle: GoogleFonts.ubuntu(
          textStyle: const TextStyle(
            color: ColorsPallete.black,
            fontWeight: FontWeight.bold,
            fontSize: 16,
          ),
        ),
        itemTextPadding: const EdgeInsets.only(left: 30),
        selectedItemTextPadding: const EdgeInsets.only(left: 30),
        itemDecoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
        ),
        selectedItemDecoration: const BoxDecoration(
          image: DecorationImage(
            alignment: Alignment.centerRight,
            fit: BoxFit.fitHeight,
            image: AssetImage('assets/images/line_orange.png'),
          ),
        ),
        iconTheme: const IconThemeData(
          color: ColorsPallete.grey,
          size: 20,
        ),
        selectedIconTheme: const IconThemeData(
          color: ColorsPallete.orange,
          size: 20,
        ),
      ),
      extendedTheme: const SidebarXTheme(
        width: 200,
        decoration: BoxDecoration(
          color: ColorsPallete.white,
        ),
      ),
      footerDivider: divider,
      headerBuilder: (context, extended) {
        return SizedBox(
          height: 100,
          child: extended
              ? SvgPicture.asset(
                  'assets/images/logoBrand.svg',
                  semanticsLabel: 'logoBrand',
                  fit: BoxFit.fitWidth,
                   alignment: Alignment.center,
                )
              : SvgPicture.asset(
                  alignment: Alignment.center,
                  fit: BoxFit.fitWidth,
                  'assets/images/menuLogo.svg',
                  semanticsLabel: 'iconLogo',
                ),
        );
      },
      items: [
        SidebarXItem(
          icon: Icons.home_outlined,
          label: 'Home',
          onTap: () {
            debugPrint('Home');
          },
        ),
        const SidebarXItem(
          icon: Icons.restaurant_menu_outlined,
          label: 'Cardápio',
        ),
        const SidebarXItem(
          icon: Icons.list_alt_outlined,
          label: 'Meus pedidos',
        ),
        const SidebarXItem(
          icon: Icons.shopping_cart_outlined,
          label: 'Carrinho',
        ),
      ],
    );
  }
}

