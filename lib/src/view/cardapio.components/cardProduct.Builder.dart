import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../shared/constanst.dart';
import '../../../stores/select_product_store.dart';
import 'productDialog.dart';

class CardProductBuilder extends StatefulWidget {
  CardProductBuilder(
      {super.key,
      required this.title,
      required this.imgSvg,
      required this.type,
      required this.description,
      required this.price,
      required this.screenHeight,
      required this.screenWidth,
      required this.indexCard});

  String title;
  String imgSvg;
  String type;
  String description;
  String price;
  double screenHeight;
  double screenWidth;
  int indexCard;

  @override
  State<CardProductBuilder> createState() => _CardProductBuilderState();
}

final productStore = SelectProductStore();

class _CardProductBuilderState extends State<CardProductBuilder> {
  @override
  Widget build(BuildContext context) {
    bool isHover = false;

    return Observer(builder: (_) {
      return SizedBox(
        child: ElevatedButton(
          onPressed: () async {
            productStore.resetValues();
            showDialog(
              context: context,
              builder: (BuildContext context) => ProductDialog(
                  title: widget.title,
                  price: widget.price,
                  productStore: productStore),
            );
          },
          onHover: (value) {
            productStore.change(value, widget.indexCard);
            if (isHover == true) {
              productStore.change(value, -1);
            }
            isHover = productStore.isHoverGet;
          },
          style: ButtonStyle(
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            )),
            overlayColor: MaterialStateProperty.resolveWith<Color?>(
              (Set<MaterialState> states) {
                if (states.contains(MaterialState.hovered)) {
                  return ColorsPallete.orange;
                }
                return null;
              },
            ),
            backgroundColor: MaterialStateProperty.all(ColorsPallete.white),
            foregroundColor: MaterialStateProperty.resolveWith<Color?>(
              (Set<MaterialState> states) {
                if (states.contains(MaterialState.hovered)) {
                  return ColorsPallete.white;
                }
                return ColorsPallete.black;
              },
            ),
            surfaceTintColor: MaterialStateProperty.resolveWith<Color?>(
              (Set<MaterialState> states) {
                if (states.contains(MaterialState.hovered)) {
                  return Colors.red;
                }
                return Colors.green;
              },
            ),
          ),
          child: Container(
            padding:
                const EdgeInsets.only(top: 20, bottom: 20, left: 10, right: 10),
            width: double.infinity,
            alignment: Alignment.centerLeft,

            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      widget.title,
                      style: GoogleFonts.ubuntu(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  width: double.infinity,
                  child: Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    alignment: WrapAlignment.spaceBetween,
                    runAlignment: WrapAlignment.center,
                    children: [
                      Container(
                        child: Text(
                          widget.description,
                          style: GoogleFonts.ubuntu(
                            fontSize: 16,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
                      Wrap(
                        crossAxisAlignment: WrapCrossAlignment.center,
                        runAlignment: WrapAlignment.center,
                        spacing: 10,
                        children: [
                          Text(
                            widget.price,
                            style: GoogleFonts.ubuntu(
                              fontSize: 16,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SvgPicture.asset(
                            widget.imgSvg,
                            height: 50,
                            width: 50,
                            semanticsLabel: 'logoBrand',
                            color: productStore.isHoverGet &&
                                    productStore.indexCardGet ==
                                        widget.indexCard
                                ? ColorsPallete.white
                                : ColorsPallete.orange,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            // ListTile(
            //   enableFeedback: true,
            //   title: Text(
            //     widget.title,
            //   ),
            //   leading: const SizedBox(
            //     width: 10,
            //     height: double.infinity,
            //   ),
            //   subtitle: Row(
            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //     children: [
            //       Text(widget.description),
            //       Text(widget.price),
            //     ],
            //   ),
            //   trailing: Observer(builder: (_) {
            //     return SvgPicture.asset(
            //       widget.imgSvg,
            //       semanticsLabel: 'logoBrand',
            //       color: isHover != true
            //           ? ColorsPallete.orange
            //           : ColorsPallete.white,
            //     );
            //   }),
            // ),
          ),
        ),
      );
    });
  }
}

    // return Observer(builder: (_) {
    //   return Ink(
    //     height: widget.screenHeight * 0.1,
    //     width: widget.screenWidth * 0.6,
    //     decoration: BoxDecoration(
    //       color: ColorsPallete.white,
    //       borderRadius: BorderRadius.circular(20),
    //       image: const DecorationImage(
    //         image: AssetImage("assets/images/product_card_bg.png"),
    //         fit: BoxFit.fill,
    //       ),
    //     ),
    //     child: InkWell(
    //       onTap: () async {
    //         showDialog(
    //             context: context,
    //             builder: (BuildContext context) => AlertDialog(
    //                   content: Column(
    //                     children: [
    //                       Text(widget.title),
    //                       Text(widget.price),
    //                       Text(widget.description),
    //                     ],
    //                   ),
    //                   actions: <Widget>[
    //                     CupertinoButton(
    //                         child: Text('Okay'),
    //                         onPressed: () {
    //                           Navigator.pop(context);
    //                         })
    //                   ],
    //                 ));
    //       },
    //       onHover: (val) {
    //         setState(() {
    //           isHover = productStore.inkHover(val);
    //           print(isHover);
    //         });
    //       },
    //       hoverColor: ColorsPallete.orange,
    //       borderRadius: BorderRadius.circular(20),
    //       child: Center(
    //         child: ListTile(
    //           enableFeedback: true,
    //           title: Text(
    //             widget.title,
    //           ),
    //           leading: const SizedBox(
    //             width: 10,
    //             height: double.infinity,
    //           ),
    //           subtitle: Row(
    //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //             children: [
    //               Text(widget.description),
    //               Text(widget.price),
    //             ],
    //           ),
    //           trailing: Observer(builder: (_) {
    //             return SvgPicture.asset(
    //               widget.imgSvg,
    //               semanticsLabel: 'logoBrand',
    //               color: isHover != true
    //                   ? ColorsPallete.orange
    //                   : ColorsPallete.white,
    //             );
    //           }),
    //         ),
    //       ),
    //     ),
    //   );
    // });
