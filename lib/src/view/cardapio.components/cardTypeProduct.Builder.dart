
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../shared/constanst.dart';
import '../cardapio.dart';

Widget buildCard(String title, String imgSvg, String productType,
        BuildContext context) =>
    Ink(
      width: 270,
      height: 350,
      decoration: BoxDecoration(
        color: ColorsPallete.white,
        borderRadius: BorderRadius.circular(20),
        image: const DecorationImage(
          image: AssetImage("assets/images/card_bg.png"),
          fit: BoxFit.fill,
        ),
      ),
      child: InkWell(
        borderRadius: BorderRadius.circular(20),
        onTap: () {
          selectStore.change(productType);
          log("Valor: ${selectStore.productTypeGet}");
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(
              alignment: Alignment.center,
              width: 130,
              imgSvg,
              semanticsLabel: 'foodLogo',
            ),
            const SizedBox(
              height: 30,
            ),
            Text(
              title,
              style: GoogleFonts.ubuntu(
                color: ColorsPallete.grey,
                fontSize: 30,
                fontWeight: FontWeight.normal,
              ),
            ),
          ],
        ),
      ),
    );
