import 'dart:developer';
import 'dart:ui';

import 'package:anota_ai/shared/constanst.dart';
import 'package:anota_ai/src/view/product.components/optional_checkbox_component.dart';
import 'package:anota_ai/src/view/product.components/product_size_component.dart';
import 'package:anota_ai/src/view/product.components/quantity_counter_component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../stores/select_product_store.dart';
import '../cardapio.dart';

class ProductDialog extends StatefulWidget {
  ProductDialog(
      {Key? key,
      required this.title,
      required this.price,
      required this.productStore})
      : super(key: key);

  SelectProductStore productStore;
  String title;
  String price;

  @override
  State<ProductDialog> createState() => _ProductDialogState();
}

var selectedSizeIndex = 1;

class _ProductDialogState extends State<ProductDialog> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(50.0)),
      ),
      backgroundColor: ColorsPallete.white,
      title: Center(
        child: Text(
          widget.title,
          style: GoogleFonts.ubuntu(
            fontSize: 24,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      content: Observer(builder: (_) {
        return SizedBox(
          width: 500,
          child: ListView(
            shrinkWrap: true,
            children: [
              Container(
                padding: const EdgeInsets.only(top: 10),
                width: double.infinity,
                child: Wrap(
                  direction: Axis.horizontal,
                  alignment: WrapAlignment.start,
                  runAlignment: WrapAlignment.center,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    Text(
                      "Selecione o tamanho: ",
                      style: GoogleFonts.ubuntu(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: ColorsPallete.black75Opacity,
                      ),
                    ),
                    ProductSizeWidget(productStore: widget.productStore),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.only(top: 10),
                width: double.infinity,
                child: Wrap(
                  crossAxisAlignment: WrapCrossAlignment.start,
                  children: [
                    Text(
                      "Quantidade: ",
                      style: GoogleFonts.ubuntu(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: ColorsPallete.black50Opacity,
                      ),
                    ),
                    ProductQuantityWidget(
                        productStore: widget.productStore, price: widget.price),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.only(top: 10),
                width: double.infinity,
                child: Wrap(
                  alignment: WrapAlignment.start,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    Text(
                      "Valor: ",
                      style: GoogleFonts.ubuntu(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: ColorsPallete.black50Opacity,
                      ),
                    ),
                    Text(
                      widget.productStore.getTotalAmount != ""
                          ? "R\$ ${widget.productStore.getTotalAmount}"
                          : "R\$ ${widget.price}",
                      style: GoogleFonts.ubuntu(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: ColorsPallete.black,
                      ),
                    ),
                  ],
                ),
              ),
              // Column(
              //   mainAxisAlignment: MainAxisAlignment.start,
              //   children: [
              //     Text(
              //       "Opcionais: ",
              //       textAlign: TextAlign.start,
              //       style: GoogleFonts.ubuntu(
              //         fontSize: 18,
              //         fontWeight: FontWeight.bold,
              //         color: ColorsPallete.black,
              //       ),
              //     ),
              //     OptionalsCheckboxList(productStore: widget.productStore),
              //   ],
              // ),
              Padding(
                padding: const EdgeInsets.only(top: 20, bottom: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Observações:",
                      style: GoogleFonts.ubuntu(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: ColorsPallete.black50Opacity,
                      ),
                    ),
                    const SizedBox(
                      width: double.infinity,
                      height: 80,
                      child: TextField(
                        decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 1,
                              color: ColorsPallete.lightGrey,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 1,
                              color: ColorsPallete.lightGrey,
                            ),
                          ),
                        ),
                        keyboardType: TextInputType.multiline,
                        maxLines: null,
                        expands: true,
                      ),
                    ),
                  ],
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(top: 10.0, left: 20, right: 20),
                child: ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    onPrimary: ColorsPallete.orange,
                    primary: ColorsPallete.white,
                    side: const BorderSide(
                      width: 3.0,
                      color: ColorsPallete.orange,
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    selectStore.change("");
                    log("Valor: ${selectStore.productTypeGet}");
                  },
                  icon: const Icon(Icons.shopping_cart),
                  label: Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      "Adicionar ao carrinho",
                      style: GoogleFonts.ubuntu(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: ColorsPallete.orange,
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0, left: 20, right: 20),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    primary: ColorsPallete.orange,
                    side: const BorderSide(
                      width: 2.0,
                      color: ColorsPallete.orange,
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    selectStore.change("");
                    log("Valor: ${selectStore.productTypeGet}");
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: Text(
                      "Finalizar compra",
                      style: GoogleFonts.ubuntu(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: ColorsPallete.white,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }),
    );
  }
}
