import 'dart:developer';

import 'package:anota_ai/class/product.class.dart';
import 'package:anota_ai/shared/constanst.dart';
import 'package:anota_ai/src/view/cardapio.components/cardProduct.Builder.dart';
import 'package:flutter/material.dart';

import '../cardapio.dart';

class ProductList extends StatefulWidget {
  ProductList({Key? key, required this.productType}) : super(key: key);
  String productType;

  @override
  State<ProductList> createState() => _ProductListState();
}

Product quatroQueijos = Product(
  title: "Quatro Queijos",
  img: "assets/images/products_icons/pizza.svg",
  type: "pizzas",
  description: 'Quatro queijos diferentes',
  price: '25.90',
);

Product portuguesa = Product(
  title: "Portuguesa",
  img: "assets/images/products_icons/pizza.svg",
  type: "pizzas",
  description:
      'Molho de tomate, mussarela, tiras de presunto, cebola e azeitonas verdes cobertas com cream cheese.',
  price: '25.90',
);

Product pepperoni = Product(
  title: "Pepperoni",
  img: "assets/images/products_icons/pizza.svg",
  type: "pizzas",
  description:
      'Fatias de pepperoni servidas sobre generosa camada de queijo hut e molho de tomate',
  price: '30.00',
);

Product smash = Product(
  title: "Smash",
  img: "assets/images/products_icons/pizza.svg",
  type: "lanches",
  description: 'Pão de briche, duas carnes, fatia de queijo cheedar',
  price: '20.00',
);

class _ProductListState extends State<ProductList> {
  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    List<Product> productsList = [quatroQueijos, portuguesa, pepperoni, smash];
    int _selectedIndex = 0;

    return Column(
      children: [
        Container(
          alignment: Alignment.centerLeft,
          padding: const EdgeInsets.all(10),
          child: ElevatedButton(
            style: TextButton.styleFrom(
              backgroundColor: ColorsPallete.orange,
            ),
            onPressed: () {
              selectStore.change("");
              log("Valor: ${selectStore.productTypeGet}");
            },
            child: const Text("Voltar"),
          ),
        ),
        Container(
          height: screenHeight - 100,
          width: screenWidth * 0.6,
          child: ListView.separated(
            itemCount: productsList.length,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              final item = productsList[index];
              return item.type == widget.productType
                  ? CardProductBuilder(
                      title: item.title,
                      imgSvg: item.img,
                      type: item.type,
                      description: item.description,
                      price: item.price,
                      screenHeight: screenHeight,
                      screenWidth: screenWidth,
                      indexCard: index,
                    )
                  : const Text("");
            },
            separatorBuilder: (context, index) {
              return const SizedBox(height: 15);
            },
          ),
        )
      ],
    );
  }
}
