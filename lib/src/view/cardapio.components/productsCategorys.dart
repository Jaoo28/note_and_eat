import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../../../class/ProductType.class.dart';
import 'cardTypeProduct.Builder.dart';

class ProductsCategorys extends StatefulWidget {
  const ProductsCategorys({Key? key}) : super(key: key);

  @override
  State<ProductsCategorys> createState() => _ProductsCategorysState();
}

ProductType pizzas = ProductType(
  title: "Pizzas",
  img: "assets/images/food_icons/pizza.icon.svg",
  type: "pizzas",
);

ProductType lanches = ProductType(
  title: "Lanches",
  img: "assets/images/food_icons/burguer.icon.svg",
  type: "lanches",
);

ProductType drinks = ProductType(
  title: "Drinks",
  img: "assets/images/food_icons/drink.icon.svg",
  type: "drinks",
);

ProductType bebidas = ProductType(
  title: "Bebidas",
  img: "assets/images/food_icons/juice.icon.svg",
  type: "bebidas",
);

ProductType sobremesas = ProductType(
  title: "Sobremesas",
  img: "assets/images/food_icons/icecream.icon.svg",
  type: "sobremesas",
);

ProductType porcoes = ProductType(
  title: "Porções",
  img: "assets/images/food_icons/fries.icon.svg",
  type: "porcoes",
);

List<ProductType> myList = [
  pizzas,
  lanches,
  drinks,
  bebidas,
  sobremesas,
  porcoes
];

class _ProductsCategorysState extends State<ProductsCategorys> {
  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;
    final isBigScreen = MediaQuery.of(context).size.width >= 768;

    return Container(
      padding: const EdgeInsets.only(top: 10, bottom: 10),
      alignment: Alignment.center,
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        controller: ScrollController(),
        child: Container(
          width: isBigScreen ? screenWidth * 0.6 : double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Wrap(
                spacing: 40,
                runSpacing: 20,
                children: [
                  buildCard(
                      myList[0].title, myList[0].img, myList[0].type, context),
                  buildCard(
                      myList[1].title, myList[1].img, myList[1].type, context),
                  buildCard(
                      myList[2].title, myList[2].img, myList[2].type, context),
                  buildCard(
                      myList[3].title, myList[3].img, myList[3].type, context),
                  buildCard(
                      myList[4].title, myList[4].img, myList[4].type, context),
                  buildCard(
                      myList[5].title, myList[5].img, myList[5].type, context),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
