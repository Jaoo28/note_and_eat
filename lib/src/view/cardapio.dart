import 'dart:developer';

import 'package:anota_ai/src/view/cardapio.components/productsList.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../../stores/select_product_type_store.dart';
import 'cardapio.components/productsCategorys.dart';

class CardapioPage extends StatefulWidget {
  const CardapioPage({Key? key}) : super(key: key);

  @override
  State<CardapioPage> createState() => _CardapioPageState();
}

final selectStore = SelectProductTypeStore();

class _CardapioPageState extends State<CardapioPage> {
  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    log("Altura da tela $screenHeight, largura da tela $screenWidth, ");

    return Observer(builder: (_) {
      return selectStore.productTypeGet != ""
          ? ProductList(productType: selectStore.productTypeGet)
          : const ProductsCategorys();
    });
  }
}
