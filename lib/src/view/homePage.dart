import 'package:anota_ai/src/view/sidebar.dart';
import 'package:flutter/material.dart';
import 'package:sidebarx/sidebarx.dart';
import 'package:anota_ai/shared/constanst.dart';

import 'contentPage.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _controller = SidebarXController(selectedIndex: 0, extended: true);

  final _key = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Anota ai',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: ColorsPallete.white,
        canvasColor: ColorsPallete.white,
        scaffoldBackgroundColor: ColorsPallete.whitePastel,
        textTheme: const TextTheme(
          headline5: TextStyle(
            color: Colors.white,
            fontSize: 46,
            fontWeight: FontWeight.w800,
          ),
        ),
      ),
      home: Builder(
        builder: (context) {
          final pageTitle = _getTitleByIndex(_controller.selectedIndex);
          final isSmallScreen = MediaQuery.of(context).size.width < 600;
          return Scaffold(
            key: _key,
            appBar: isSmallScreen
                ? AppBar(
                    backgroundColor: ColorsPallete.white,
                    title: Text(pageTitle),
                    leading: IconButton(
                      onPressed: () {
                        // if (!Platform.isAndroid && !Platform.isIOS) {
                        //   _controller.setExtended(true);
                        // }
                        _key.currentState?.openDrawer();
                      },
                      icon: const Icon(
                        Icons.menu,
                        color: ColorsPallete.orange,
                      ),
                    ),
                  )
                : null,
            drawer: SideBar(controller: _controller),
            body: Row(
              children: [
                if (!isSmallScreen) SideBar(controller: _controller),
                Expanded(
                  child: Center(
                    child: contentPage(
                      controller: _controller,
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}

String _getTitleByIndex(int index) {
  switch (index) {
    case 0:
      return 'Home';
    case 1:
      return 'Cardápio';
    case 2:
      return 'Meus Pedidos';
    case 3:
      return 'Carrinho'; 
    default:
      return 'Not found page';
  }
}
