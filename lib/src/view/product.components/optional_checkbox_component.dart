import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../../../stores/select_product_store.dart';

class OptionalsCheckboxList extends StatefulWidget {
  OptionalsCheckboxList({super.key, required this.productStore});

  SelectProductStore productStore;

  @override
  State<OptionalsCheckboxList> createState() => _OptionalsCheckboxListState();
}

List<String> optionals = ["Cebola", "Azeitona", "Catupiry", "Cheedar"];

class _OptionalsCheckboxListState extends State<OptionalsCheckboxList> {
  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      return ListView.builder(
          itemCount: optionals.length,
          shrinkWrap: true,
          itemBuilder: (context, i) {
            return ListTile(
                title: Text(optionals[i]),
                leading: Checkbox(
                  value: widget.productStore.getOptionalsChecked
                      .contains(optionals[i]),
                  onChanged: (val) {
                    if (val == true) {
                      widget.productStore.addOptional(optionals[i]);
                    } else {
                      widget.productStore.removeOptional(optionals[i]);
                    }
                  },
                )
                //you can use checkboxlistTile too
                );
          });
    });
  }
}
