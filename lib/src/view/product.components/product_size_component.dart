import 'dart:developer';

import 'package:anota_ai/shared/constanst.dart';
import 'package:anota_ai/src/view/cardapio.components/cardProduct.Builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:simple_shadow/simple_shadow.dart';

import '../../../stores/select_product_store.dart';

class ProductSizeWidget extends StatefulWidget {
  ProductSizeWidget({super.key, required this.productStore});

  SelectProductStore productStore;
  @override
  State<ProductSizeWidget> createState() => _ProductSizeWidgetState();
}

class _ProductSizeWidgetState extends State<ProductSizeWidget> {
  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      return Container(
        width: 140,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 5.0, right: 5.0),
              child: GestureDetector(
                onTap: () {
                  productStore.changeProductSize("P");
                  log("Troca para P:${productStore.productSize}");
                  // setState(() {
                  //   sizeSelected = "P";
                  // });
                },
                child: productStore.getProductSize != "P"
                    ? SimpleShadow(
                        child: SvgPicture.asset(
                          "images/product_size_icons/p_unselected.svg",
                          width: 35,
                          height: 35,
                        ), // Default: 2
                      )
                    : SimpleShadow(
                        child: SvgPicture.asset(
                          "images/product_size_icons/p_selected.svg",
                          width: 35,
                          height: 35,
                        ),
                      ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 5.0, right: 5.0),
              child: GestureDetector(
                onTap: () {
                  productStore.changeProductSize("M");
                  log("Troca para M:${productStore.productSize}");
                  // setState(() {
                  //   sizeSelected = "M";
                  // });
                },
                child: productStore.getProductSize != "M"
                    ? SimpleShadow(
                        child: SvgPicture.asset(
                          "images/product_size_icons/m_unselected.svg",
                          width: 35,
                          height: 35,
                        ),
                      )
                    : SimpleShadow(
                        child: SvgPicture.asset(
                          "images/product_size_icons/m_selected.svg",
                          width: 35,
                          height: 35,
                        ),
                      ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 5.0, right: 5.0),
              child: GestureDetector(
                onTap: () {
                  productStore.changeProductSize("G");
                  log("Troca para G:${productStore.productSize}");
                  // setState(() {
                  //   sizeSelected = "G";
                  // });
                },
                child: productStore.getProductSize != "G"
                    ? SimpleShadow(
                        child: SvgPicture.asset(
                          "images/product_size_icons/g_unselected.svg",
                          width: 35,
                          height: 35,
                        ),
                      )
                    : SimpleShadow(
                        child: SvgPicture.asset(
                          "images/product_size_icons/g_selected.svg",
                          width: 35,
                          height: 35,
                        ),
                      ),
              ),
            ),
          ],
        ),
      );
    });
  }
}
