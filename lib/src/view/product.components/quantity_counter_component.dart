import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../shared/constanst.dart';
import '../../../stores/select_product_store.dart';

class ProductQuantityWidget extends StatefulWidget {
  ProductQuantityWidget(
      {super.key, required this.productStore, required this.price});

  SelectProductStore productStore;
  String price;

  @override
  State<ProductQuantityWidget> createState() => _ProductQuantityWidgetState();
}

class _ProductQuantityWidgetState extends State<ProductQuantityWidget> {
  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      return SizedBox(
        width: 180,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                if (widget.productStore.getProductCounter != 1) {
                  widget.productStore.removeProductQuantity(widget.price);
                }
              },
              style: ElevatedButton.styleFrom(
                primary: Colors.transparent,
                shadowColor: Colors.transparent,
              ),
              child: const Icon(
                Icons.remove,
                color: ColorsPallete.orange,
                size: 30,
              ),
            ),
            Container(
              padding:
                  const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
              decoration: const BoxDecoration(
                  color: ColorsPallete.orange,
                  borderRadius: BorderRadius.all(Radius.circular(8))),
              child: Text(
                "${widget.productStore.getProductCounter}",
                style: GoogleFonts.ubuntu(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: ColorsPallete.white,
                ),
              ),
            ),
            ElevatedButton(
              onPressed: () {
                widget.productStore.addProductQuantity(widget.price);
              },
              style: ElevatedButton.styleFrom(
                primary: Colors.transparent,
                shadowColor: Colors.transparent,
              ),
              child: const Icon(
                Icons.add,
                color: ColorsPallete.orange,
                size: 30,
              ),
            ),
          ],
        ),
      );
    });
  }
}
