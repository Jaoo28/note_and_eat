class Product {
  String title;
  String img;
  String type;
  String description;
  String price;

  Product(
      {required this.title,
      required this.img,
      required this.type,
      required this.description,
      required this.price});
}
