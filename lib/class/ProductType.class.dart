class ProductType {
  String title;
  String img;
  String type;

  ProductType({required this.title, required this.img, required this.type});
}
